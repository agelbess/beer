# How to run

The program is build in Java with Maven and JDK 1.8.

Execute `mvn clean install` in the root directory and then `java -jar target\beer-0.0.1-jar-with-dependencies.jar`.

Optionally, use arguments. For example:

`java -jar target\beer-0.0.1-jar-with-dependencies.jar 51.355468 11.100790 2000`

After the execution, the program will print the results in the console and also create three KML files in the user's folder. Sample KML files are in the assets folder.

The KML files can be uploaded in google maps for inspection.
([sample](https://drive.google.com/open?id=11wLkQfr9Z7wBjpgKyE8gT79Hk2AXqFu7&usp=sharing]))

The output with the default arguments is in the assest/out.txt file.

Thank you,

Alexandros Gelbessis