package gr.gelbessis.beer.core.util;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class UtilsTest {

    @Test
    public void testRound () {
        assertEquals(new Double(10.0), new Double(Utils.round(10.0, 1)));
        assertEquals(new Double(10.1), new Double(Utils.round(10.1, 1)));
        assertEquals(new Double(10.10), new Double(Utils.round(10.10, 2)));
        assertEquals(new Double(10.10), new Double(Utils.round(10.100, 2)));
        assertEquals(new Double(10.10), new Double(Utils.round(10.101, 2)));
        assertEquals(new Double(10.10), new Double(Utils.round(10.123, 1)));
    }

    @Test
    public void testToColor () {
        assertEquals("00", Utils.toColor(0, 1));
        assertEquals("0F", Utils.toColor(15, 256));
        assertEquals("10", Utils.toColor(16, 256));
        assertEquals("FF", Utils.toColor(255, 256));
    }
}
