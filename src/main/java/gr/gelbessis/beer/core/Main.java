package gr.gelbessis.beer.core;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import gr.gelbessis.beer.core.model.Beer;
import gr.gelbessis.beer.core.model.Brewery;
import gr.gelbessis.beer.core.model.Geocode;
import gr.gelbessis.beer.core.service.BreweryService;
import gr.gelbessis.beer.core.service.BreweryServiceException;
import gr.gelbessis.beer.core.service.BreweryServiceSolveParams;
import gr.gelbessis.beer.core.service.BreweryServiceSolveResult;
import gr.gelbessis.beer.core.service.BreweryServiceStartResult;
import gr.gelbessis.beer.core.service.Node;
import gr.gelbessis.beer.core.util.KmlUtils;
import gr.gelbessis.beer.core.util.ModelLoader;
import gr.gelbessis.beer.core.util.Utils;

public class Main {
	private final static Logger LOGGER = Logger.getLogger(Main.class.getName());

	/**
	 * Solves the problem, prints the results and creates 3 KML files in the user's
	 * folder.
	 * 
	 * The first contains all the Breweries, the second only the ones within range
	 * of 2000 fuel kilometers (1000km radius) and the third contains the solution
	 * path.
	 * 
	 * 
	 * Accepts three (3) arguments. The HOME latitude, the HOME longitude and the
	 * fuel distance. END latitude/longitude are the same as HOME's.
	 */
	public static void main(String[] args) throws IOException, InstantiationException, IllegalAccessException,
			InvocationTargetException, BreweryServiceException {

		/*
		 * Here are the default values required by the "Beer Travel Mission"
		 */
		double homeLat = 51.355468;
		double homeLon = 11.100790;
		double endLat = 51.355468;
		double endLon = 11.100790;
		double fuelDistance = 2000;

		if (args.length > 0) {
			homeLat = Double.parseDouble(args[0]);
			endLat = homeLat;
			println("Set HOME lat: " + homeLat);
			if (args.length > 1) {
				homeLon = Double.parseDouble(args[1]);
				endLon = homeLon;
				println("Set HOME lon: " + homeLon);
				if (args.length > 2) {
					fuelDistance = Double.parseDouble(args[2]);
					println("Set fuel distance: " + fuelDistance);
				}
			}
		}

		/*
		 * The algorithm uses the beerFactor variable and the distanceFactor variable.
		 * In order to find the best solution, the program tries many values around the
		 * factorBase.
		 * 
		 * E.g. for factorBase 0.9, factorRange 0.1 and factorStep 0.01, the program
		 * will execute 21 times with factorValues from 0.8 to 1.0 with step 0.01.
		 */
		double factorBase = 0.9;
		double factorRange = 0.1;
		double factorStep = 0.01;

		/*
		 * The program does not use a database. The data are not large and can fit in
		 * memory pretty fine.
		 */
		Map<Long, Brewery> breweries;
		Map<Long, Beer> beers;
		Map<Long, Geocode> geocodes;

		{
			InputStream is = Main.class.getResourceAsStream("/breweries.csv");
			breweries = ModelLoader.loadCSV(Brewery.class, is);
		}
		{
			InputStream is = Main.class.getResourceAsStream("/beers.csv");
			beers = ModelLoader.loadCSV(Beer.class, is);
			for (Beer beer : beers.values()) {
				Brewery brewery = breweries.get(beer.getBreweryId());
				brewery.setBeers(brewery.getBeers() + 1);
			}
		}
		{
			InputStream is = Main.class.getResourceAsStream("/geocodes.csv");
			geocodes = ModelLoader.loadCSV(Geocode.class, is);
			for (Geocode geocode : geocodes.values()) {
				Brewery brewery = breweries.get(geocode.getBreweryId());
				if (brewery != null) {
					brewery.setLat(geocode.getLat());
					brewery.setLon(geocode.getLon());
				}
			}
		}
		Set<Long> toRemove = new HashSet<>();
		for (Long breweryId : breweries.keySet()) {
			Brewery brewery = breweries.get(breweryId);
			if (brewery.getLat() == null || brewery.getLon() == null) {
				LOGGER.warning("Missing brewery geolocation " + brewery);
				toRemove.add(breweryId);
			} else if (brewery.getBeers() == 0.0) {
				LOGGER.warning("Missing beers " + brewery);
				toRemove.add(breweryId);
			}
		}
		for (Long removeIt : toRemove) {
			breweries.remove(removeIt);
		}

		Calendar cal = new GregorianCalendar();
		String now = String.format("%02d", cal.get(Calendar.HOUR)) + String.format("%02d", cal.get(Calendar.MINUTE))
				+ String.format("%02d", cal.get(Calendar.SECOND));

		BreweryServiceSolveParams solveParams = new BreweryServiceSolveParams();
		solveParams.setHomeLat(homeLat);
		solveParams.setHomeLon(homeLon);
		solveParams.setEndLat(endLat);
		solveParams.setEndLon(endLon);
		solveParams.setFuelDistance(fuelDistance);
		solveParams.setFactorBase(factorBase);
		solveParams.setFactorRange(factorRange);
		solveParams.setFactorStep(factorStep);
		solveParams.setBreweries(new ArrayList<>(breweries.values()));
		Long startTime = System.currentTimeMillis();
		BreweryService breweryService = new BreweryService(solveParams);
		BreweryServiceSolveResult solveResult = breweryService.solve();

		/*
		 * The result contains the best solution, meaning the one with the most beer
		 * types
		 */
		BreweryServiceStartResult result = solveResult.getStartResult();
		/*
		 * Start formal output
		 */
		println("Found " + (result.getHome().getTotalNodes() - 2) + " beer factories: ");
		Node next = result.getHome();
		List<Long> visitedBreweries = new ArrayList<>();
		while (next != null) {
			visitedBreweries.add(next.getBrewery().getId());
			long id = next.getBrewery().getId();
			println("\t-> " + (id < 0L ? "" : "[" + id + "] ") + next.getBrewery().getName() //
					+ " " + next.getLat() + ", " + next.getLon() //
					+ " distance " + (int) next.getNextDistance() + " km." //
			);
			next = next.getNext();
		}
		println("");
		println("Total distance traveled: " + (int) result.getHome().getTotalDistance() + " km.\n");
		println("Collected " + solveResult.getStartResult().getHome().getTotalBeers() + " beer types:");

		List<Beer> collectedBeers = beers.values().stream().filter(b -> visitedBreweries.contains(b.getBreweryId()))
				.collect(Collectors.toList());
		collectedBeers.sort(new Comparator<Beer>() {
			@Override
			public int compare(Beer o1, Beer o2) {
				return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
			}
		});
		for (Beer beer : collectedBeers) {
			println("\t-> " + beer.getName());
		}
		Long endTime = System.currentTimeMillis();
		println("Program took: " + (endTime - startTime) + " ms for " + solveResult.getIterations() + " iterations");

		/*
		 * Writing KML files in user's home directory
		 */

		// Create a file with all the breweries
		Kml kmlAll = new Kml();
		Document kmlAllDocument = kmlAll.createAndSetDocument();
		for (Brewery brewery : breweries.values()) {
			KmlUtils.append(kmlAllDocument, brewery);
		}
		String klmAllPath = System.getProperty("user.home") //
				+ "\\beer" //
				+ "-t" + now//
				+ "all.kml";
		LOGGER.info("Creating KML " + klmAllPath);
		kmlAll.marshal(new File(klmAllPath));

		// Create a file with the Breweries within the 2000 fuel distance
		Kml kmlInRange = new Kml();
		Document kmlInRangeDocument = kmlInRange.createAndSetDocument();
		for (Brewery brewery : solveResult.getInRangeBreweries()) {
			KmlUtils.append(kmlInRangeDocument, brewery);
		}
		String klmInRangePath = System.getProperty("user.home") //
				+ "\\beer" //
				+ "-t" + now//
				+ "-f" + solveParams.getFuelDistance() //
				+ "-lat" + Utils.round(solveParams.getHomeLat(), 4) //
				+ "-lon" + Utils.round(solveParams.getHomeLon(), 4) //
				+ "inRange.kml";
		LOGGER.info("Creating Range KML " + klmInRangePath);
		kmlInRange.marshal(new File(klmInRangePath));

		Kml kmlN = new Kml();
		Document kmlNDocument = kmlN.createAndSetDocument();
		String lineColor = "#FF" + Utils.toColor(result.getStartParams().getDistanceFactor(), 1) + "FF"
				+ Utils.toColor(result.getStartParams().getBeerFactor(), 1);
		KmlUtils.append(kmlNDocument, result.getHome(), lineColor);

		// Create a file with the Breweries of the best solution
		String klmNPath = System.getProperty("user.home") //
				+ "\\beer" //
				+ "-t" + now//
				+ "-f" + solveParams.getFuelDistance() //
				+ "-lat" + Utils.round(solveParams.getHomeLat(), 4) //
				+ "-lon" + Utils.round(solveParams.getHomeLon(), 4) //
				+ "-cB" + result.getHome().getTotalBeers()//
				+ "-cN" + (result.getHome().getTotalNodes() - 2)//
				+ "-d" + Utils.round(result.getStartParams().getDistanceFactor(), 2) //
				+ "-b" + Utils.round(result.getStartParams().getBeerFactor(), 2) //
				+ ".kml";
		LOGGER.info("Creating Solution KML " + klmNPath);
		kmlN.marshal(new File(klmNPath));
		LOGGER.info("Done");
	}

	private static final void println(String msg) {
		System.out.println(msg);
	}
}
