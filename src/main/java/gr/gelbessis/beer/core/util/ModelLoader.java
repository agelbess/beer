package gr.gelbessis.beer.core.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import gr.gelbessis.beer.core.model.Beer;
import gr.gelbessis.beer.core.model.Brewery;
import gr.gelbessis.beer.core.model.Geocode;

public class ModelLoader {

    private static final Map <Class, String[][]> map = new HashMap <>();
    static {
        map.put(
            Brewery.class,
            new String[][] { //
                { "id", //
                    "name", //
                    "address1", //
                    "address2", //
                    "city", //
                    "state", //
                    "code", //
                    "country", //
                    "phone", //
                    "website" //
                }, { //
                    "id", //
                    "name", //
                    "address1", //
                    "address2", //
                    "city", //
                    "state", //
                    "code", //
                    "country", //
                    "phone", //
                    "website" //
                }, });
        map.put(
            Beer.class,
            new String[][] { //
                { "id", //
                    "breweryId", //
                    "name", //
                }, { //
                    "id", //
                    "brewery_id", //
                    "name", //
                },//
            });
        map.put(
            Geocode.class,
            new String[][] { //
                { "id", //
                    "breweryId", //
                    "lat", //
                    "lon"//
                }, { //
                    "id", //
                    "brewery_id", //
                    "latitude", //
                    "lontitude" }, //
            });
    }

    public static final <T> Map <Long, T> loadCSV (Class <T> clazz, InputStream is)
        throws IOException,
        InstantiationException,
        IllegalAccessException,
        InvocationTargetException {
        Map <Long, T> result = new HashMap <>();
        Reader in = new InputStreamReader(is);
        String[][] headers = map.get(clazz);
        Iterable <CSVRecord> records = CSVFormat.RFC4180.withHeader(headers[1]).parse(in);
        int recordNum = -1;
        for (CSVRecord record: records) {
            recordNum++;
            if (recordNum == 0) {
                continue;
            }
            T row = clazz.newInstance();
            for (int i = 0; i < headers[0].length; i++) {
                BeanUtils.setProperty(row, headers[0][i], record.get(headers[1][i]));
            }
            BeanUtils.setProperty(row, "row", new Long(recordNum));

            result.put(Long.parseLong(record.get(0)), row);
        }
        return result;
    }
}
