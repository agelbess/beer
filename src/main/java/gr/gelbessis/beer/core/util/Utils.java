package gr.gelbessis.beer.core.util;

public class Utils {

	public static final double round(double val, int decimals) {
		double multi = Math.pow(10.0, decimals);
		val = val * multi;
		val = Math.round(val);
		val = val / multi;
		return val;
	}

	public static String toColor(double value, double scale) {
		int i = (int) (value / scale * 256);
		return String.format("%02X", i);
	}
}
