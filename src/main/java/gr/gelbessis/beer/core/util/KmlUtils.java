package gr.gelbessis.beer.core.util;

import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import gr.gelbessis.beer.core.model.Brewery;
import gr.gelbessis.beer.core.service.Node;

public class KmlUtils {

	public static final void append(Document document, Brewery brewery) {
		Placemark placemark = document.createAndAddPlacemark();
		String name = brewery.getName() + ", " + brewery.getCity() + ", " + brewery.getCountry();
		String description = //
				"Id: " + brewery.getId() + ".\n" + "Beers: " + brewery.getBeers() + ".\n";
		placemark.withName(name).withDescription(description).withOpen(Boolean.TRUE).createAndSetPoint()
				.addToCoordinates(brewery.getLon(), brewery.getLat());
	}

	public static final void append(Document document, Node node, String lineColor) {
		Brewery brewery = node.getBrewery();
		Placemark placemark = document.createAndAddPlacemark();
		String name = brewery.getName() + (brewery.getCity() == null ? "" : ", " + brewery.getCity())
				+ (brewery.getCountry() == null ? "" : ", " + brewery.getCountry());
		String description = //
				"Id: " + node.getBrewery().getId() + ".\n"//
						+ "Beers: " + node.getBrewery().getBeers() + ".\n"//
						+ "Geolocation: lat " + Utils.round(node.getLat(), 4) + " lon." + Utils.round(node.getLon(), 4)
						+ ".\n" //
						+ "Next distance: " + node.getNextDistance() + ".\n\n"//

						+ "Distance factor: " + node.getDistanceFactorValue() + ".\n"//
						+ "Beer factor: " + node.getBeerFactorValue() + ".\n\n"//

						+ "Factor-MaxDistance: " + node.getMaxDistance() + ".\n"//
						+ "DynamicFactor-MaxBeers: " + node.getDynamicMaxBeers() + ".\n\n"//

						+ "Total beers to destination: " + node.getTotalBeers() + ".\n"//
						+ "Total breweries to destination: " + node.getTotalNodes() + ".\n"//
						+ "Total distance to destination: " + Utils.round(node.getTotalDistance(), 1) + " km.\n" //
		;
		placemark.withName(name).withDescription(description).withOpen(Boolean.TRUE).createAndSetPoint()
				.addToCoordinates(node.getLon(), node.getLat());
		placemark.createAndAddStyle().createAndSetBalloonStyle().withColor(lineColor);

		if (node.getNext() != null) {
			Brewery nextBrewery = node.getNext().getBrewery();
			String lineName = brewery.getName() + " - " + nextBrewery.getName();
			String lineDescription = "Distance is " + Utils.round(node.getNextDistance(), 1) + " km.\n";
			Placemark placemarkLine = document.createAndAddPlacemark();
			placemarkLine.createAndAddStyle().createAndSetLineStyle().withColor(lineColor).withWidth(2.0);
			placemarkLine.withName(lineName).withDescription(lineDescription).createAndSetLineString()
					.addToCoordinates(node.getLon(), node.getLat())
					.addToCoordinates(node.getNext().getLon(), node.getNext().getLat());
			append(document, node.getNext(), lineColor);
		}
	}
}
