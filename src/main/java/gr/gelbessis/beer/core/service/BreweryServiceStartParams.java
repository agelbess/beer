package gr.gelbessis.beer.core.service;

import lombok.Data;

@Data
public class BreweryServiceStartParams {
	private double distanceFactor;
	private double beerFactor;
}
