package gr.gelbessis.beer.core.service;

public class BreweryServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BreweryServiceException(String string) {
		super(string);
	}

}
