package gr.gelbessis.beer.core.service;

import java.util.List;

import gr.gelbessis.beer.core.model.Brewery;
import lombok.Data;

@Data
public class BreweryServiceSolveParams {
	private List<Brewery> breweries;
	private double homeLat;
	private double homeLon;
	private double endLat;
	private double endLon;
	private double fuelDistance;
	
    private double factorBase;
    private double factorRange;
    private double factorStep;

}
