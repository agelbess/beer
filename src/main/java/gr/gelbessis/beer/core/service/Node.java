package gr.gelbessis.beer.core.service;

import gr.gelbessis.beer.core.model.Brewery;
import lombok.Data;
import lombok.ToString;

@Data
public class Node {
	public static final Long HOME_ID = -1L;
	public static final Long END_ID = -2L;

	private double lat;
	private double lon;
	private Brewery brewery;
	@ToString.Exclude
	private Node previous;
	@ToString.Exclude
	private Node next;
	private double nextDistance = 0;
	private double homeDistance = 0;
	private double endDistance = 0;
	private double distanceFactorValue = 0;
	private double beerFactorValue = 0;
	@ToString.Exclude
	private BreweryService breweryService;
	private double dynamicMaxBeers = 0;
	private double maxDistance = 0;

	public Node(BreweryService breweryService, Brewery brewery) {
		this.breweryService = breweryService;
		this.setLat(brewery.getLat());
		this.setLon(brewery.getLon());
		this.brewery = brewery;
	}

	public void append(Node append) throws BreweryServiceException {
		append.setPrevious(this);
		Node next = getNext();
		if (next != null) {
			append.setNext(next);
			// append next distance
			Double appendNextDistance = breweryService.distance(append, next);
			append.setNextDistance(appendNextDistance);
			next.setPrevious(append);
		}
		setNext(append);
		// next distance
		Double distance = breweryService.distance(this, append);
		setNextDistance(distance);
	}

	public void remove() throws BreweryServiceException {
		Node next = this.getNext();
		this.getPrevious().append(next);
		next.setNext(null);
		next.setNextDistance(0);
	}

	public double getTotalDistance() {
		double total = getNextDistance();
		Node next = getNext();
		while (next != null) {
			total += next.getNextDistance();
			next = next.getNext();
		}
		return total;
	}

	public int getTotalBeers() {
		int total = getBrewery().getBeers();
		Node next = getNext();
		while (next != null) {
			total += next.getBrewery().getBeers();
			next = next.getNext();
		}
		return total;
	}

	public int getTotalNodes() {
		int total = 1;
		Node next = getNext();
		while (next != null) {
			total++;
			next = next.getNext();
		}
		return total;
	}
}
