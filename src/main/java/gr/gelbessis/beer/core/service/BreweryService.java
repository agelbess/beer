package gr.gelbessis.beer.core.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gr.gelbessis.beer.core.model.Brewery;
import gr.gelbessis.beer.core.util.Haversine;
import lombok.Data;

@Data
public class BreweryService {
	private static final String HOME = "Home";
	private static final String END = "Destination";

	private final BreweryServiceSolveParams solveParams;
	private final List<Brewery> breweries;
	private final Map<Long, Brewery> breweriesMap;
	/*
	 * Contains only one side of the distance. For distance(b1, b2). If the b1 is
	 * not found, try searching the b2. Same goes for distance(b2, b1).
	 * 
	 * -1 for home
	 * 
	 * -2 for end
	 */
	private final Map<Long, Map<Long, Double>> distanceMap;
	private double maxDistance = 0;
	private double maxBeers = 0;
	private final Brewery homeBrewery;
	private final Brewery endBrewery;

	public BreweryService(BreweryServiceSolveParams solveParams) {
		this.solveParams = solveParams;
		// copy
		this.breweries = new ArrayList<>(solveParams.getBreweries());
		this.breweriesMap = new HashMap<>();
		this.distanceMap = new HashMap<>();

		homeBrewery = new Brewery();
		homeBrewery.setId(Node.HOME_ID);
		homeBrewery.setName(HOME);
		homeBrewery.setLat(solveParams.getHomeLat());
		homeBrewery.setLon(solveParams.getHomeLon());
		breweries.add(homeBrewery);

		endBrewery = new Brewery();
		endBrewery.setId(Node.END_ID);
		endBrewery.setName(END);
		endBrewery.setLat(solveParams.getHomeLat());
		endBrewery.setLon(solveParams.getHomeLon());
		breweries.add(endBrewery);

		// Home
		distanceMap.put(Node.HOME_ID, new HashMap<Long, Double>());
		breweriesMap.put(Node.HOME_ID, homeBrewery);

		// End
		distanceMap.put(Node.END_ID, new HashMap<Long, Double>());
		breweriesMap.put(Node.END_ID, endBrewery);

		for (Brewery brewery : breweries) {
			// calculate the distance from home
			double homeDistance = Haversine.distance(solveParams.getHomeLat(), solveParams.getHomeLon(),
					brewery.getLat(), brewery.getLon());
			// calculate the distance from end
			double endDistance = Haversine.distance(solveParams.getEndLat(), solveParams.getEndLon(), brewery.getLat(),
					brewery.getLon());
			// we keep track of distances only for the breweries that are within the fuel
			// distance perimeter
			if (homeDistance + endDistance < solveParams.getFuelDistance()) {
				maxDistance = Math.max(maxDistance, homeDistance);
				maxDistance = Math.max(maxDistance, endDistance);
				maxBeers = Math.max(maxBeers, brewery.getBeers());
				// add the brewery in the map
				breweriesMap.put(brewery.getId(), brewery);
				// save the distance for all the previously added breweries
				for (Long mappedBreweryId : distanceMap.keySet()) {
					Brewery mappedBrewery = breweriesMap.get(mappedBreweryId);
					double distance = Haversine.distance(brewery.getLat(), brewery.getLon(), mappedBrewery.getLat(),
							mappedBrewery.getLon());
					distanceMap.get(mappedBreweryId).put(brewery.getId(), distance);
				}
				distanceMap.put(brewery.getId(), new HashMap<Long, Double>());
			}
		}
	}

	public BreweryServiceSolveResult solve() throws BreweryServiceException {
		BreweryServiceSolveResult solveResult = new BreweryServiceSolveResult();
		solveResult.setSolveParams(solveParams);

		solveResult.setInRangeBreweries(new ArrayList<>(breweriesMap.values()));

		int maxBeers = 0;
		int iterations = 0;
		/*
		 * Iterate around the factorBase with values between factorBase-factorRange and
		 * factorBase+factorRange
		 */
		for (double f = -solveParams.getFactorRange(); f <= solveParams.getFactorRange(); f += solveParams
				.getFactorStep()) {
			BreweryServiceStartParams startParams = new BreweryServiceStartParams();
			double distanceFactor = solveParams.getFactorBase() + f;
			double beerFactor = 1 - distanceFactor;
			startParams.setBeerFactor(beerFactor);
			startParams.setDistanceFactor(distanceFactor);
			BreweryServiceStartResult startResult = start(startParams, solveParams);
			if (startResult.getHome().getTotalBeers() >= maxBeers) {
				maxBeers = startResult.getHome().getTotalBeers();
				solveResult.setStartResult(startResult);
			}
			iterations++;
		}
		solveResult.setIterations(iterations);
		return solveResult;
	}

	private BreweryServiceStartResult start(BreweryServiceStartParams startParams,
			BreweryServiceSolveParams solveParams) throws BreweryServiceException {
		BreweryServiceStartResult result = new BreweryServiceStartResult();
		result.setStartParams(startParams);

		Node homeNode = new Node(this, homeBrewery);
		Node endNode = new Node(this, endBrewery);

		homeNode.append(endNode);

		// initialize the list. The nodes are brand new.
		List<Node> startNodes = new ArrayList<>();
		for (Brewery brewery : breweriesMap.values()) {
			if (!brewery.getId().equals(Node.HOME_ID) && !brewery.getId().equals(Node.END_ID)) {
				Node node = new Node(this, brewery);
				node.setHomeDistance(distance(homeNode, node));
				node.setEndDistance(distance(endNode, node));
				startNodes.add(node);
			}
		}

		Node append = homeNode;
		while (!startNodes.isEmpty()) {
			append = appendNearest(homeNode, append, startParams.getDistanceFactor(), startParams.getBeerFactor(),
					startNodes);
		}

		result.setHome(homeNode);
		return result;
	}

	/*
	 * Finds the best match node and appends it to a node of the path.
	 * 
	 * Calculates the distanceFactorValue and beerFactorValue for each node of the
	 * nodes and selects the one with the maximum sum of the these values.
	 */
	private Node appendNearest(Node homeNode, Node fromNode, double distanceFactor, double beerFactor,
			List<Node> startNodes) throws BreweryServiceException {
		double maxValue = -1;
		Node result = null;
		double resultDistanceFactorValue = 0;
		double resultBeerFactorValue = 0;
		for (Node node : startNodes) {
			double distance = distance(fromNode, node);
			double distanceFactorValue = (1 - distance / maxDistance) * distanceFactor;
			double beerFactorValue = (double) node.getBrewery().getBeers() / maxBeers * beerFactor;
			double compareValue = distanceFactorValue + beerFactorValue;
			if (compareValue > maxValue) {
				maxValue = compareValue;
				result = node;
				resultDistanceFactorValue = distanceFactorValue;
				resultBeerFactorValue = beerFactorValue;
			}
		}
		fromNode.append(result);

		if (homeNode.getTotalDistance() > solveParams.getFuelDistance()) {
			// if the distance after the new node is greater than the fuel distance then
			// revert append
			result.remove();
		} else {
			result.setDistanceFactorValue(resultDistanceFactorValue);
			result.setBeerFactorValue(resultBeerFactorValue);
			result.setDynamicMaxBeers(maxBeers);
			result.setMaxDistance(maxDistance);
			// if added, then remove it from the available nodes
			startNodes.remove(result);
		}
		return result;
	}

	public Double distance(Node n1, Node n2) throws BreweryServiceException {
		Double result = 0.0;
		if (!n1.getBrewery().getId().equals(n2.getBrewery().getId())) {
			Map<Long, Double> map = distanceMap.get(n1.getBrewery().getId());
			result = map.get(n2.getBrewery().getId());
			if (result == null) {
				map = distanceMap.get(n2.getBrewery().getId());
				result = map.get(n1.getBrewery().getId());
			}
			if (result == null) {
				throw new BreweryServiceException("Distance not found for:\n" + n1 + "\n" + n2);
			}
		}
		return result;
	}

}
