package gr.gelbessis.beer.core.service;

import java.util.List;

import gr.gelbessis.beer.core.model.Brewery;
import lombok.Data;
import lombok.ToString;

@Data
public class BreweryServiceSolveResult {
	private BreweryServiceSolveParams solveParams;
	private BreweryServiceStartResult startResult;
	@ToString.Exclude
	private List<Brewery> inRangeBreweries;
	private int iterations;
}
