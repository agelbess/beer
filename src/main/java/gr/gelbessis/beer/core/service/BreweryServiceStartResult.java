package gr.gelbessis.beer.core.service;

import lombok.Data;

@Data
public class BreweryServiceStartResult {
	private BreweryServiceStartParams startParams;
	private Node home;
}
