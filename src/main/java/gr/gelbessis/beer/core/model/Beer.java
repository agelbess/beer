package gr.gelbessis.beer.core.model;

import lombok.Data;

@Data
public class Beer {
    private Long row;
    private Long id;
    private Long breweryId;
    private String name;
}
