package gr.gelbessis.beer.core.model;

import lombok.Data;

@Data
public class Geocode {
    private Long row;
    private Long id;
    private Long breweryId;
    private Double lat;
    private Double lon;
}
