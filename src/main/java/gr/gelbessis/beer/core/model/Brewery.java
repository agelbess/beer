package gr.gelbessis.beer.core.model;

import lombok.Data;

@Data
public class Brewery {
    private Long row;
    private Long id;
    private String name;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String code;
    private String country;
    private String phone;
    private String website;
    private Double lat;
    private Double lon;
    private Integer beers = 0;
}
